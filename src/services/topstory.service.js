const axios = require('axios');
const { TopStory } = require('../models');

const createTopStory = async (id) => {
  return TopStory.create({ id });
};

const truncate = async () => {
  const topstory = await TopStory.destroy({ truncate: true });
  return topstory;
};

const getAll = async () => {
  const topstory = await TopStory.findAll();
  return topstory;
};

const syncTopStory = async () => {
  await truncate();
  const data = await axios({
    method: 'get',
    url: 'https://hacker-news.firebaseio.com/v0/topstories.json',
    responseType: 'json',
  });
  data.data.forEach(async (id) => {
    await createTopStory(id);
  });
  return getAll();
};

module.exports = {
  getAll,
  truncate,
  createTopStory,
  syncTopStory,
};
