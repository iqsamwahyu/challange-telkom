const axios = require('axios');
const { Item } = require('../models');
const itemProperties = require('../utils/itemProperties');

const getAll = async () => {
  const item = await Item.findAll({ attributes: ['id'] });
  return item;
};

const getFavorite = async () => {
  const item = await Item.findAll({ where: { favorite: true } });
  return item;
};

const putFavorite = async (id, body) => {
  await Item.update({ favorite: body.favorite }, { where: { id } });
  const item = await Item.findOne({ where: { id } });
  return itemProperties.get(item);
};

const createItem = async (item) => {
  if (item) {
    return Item.create(item);
  }
};

const truncate = async () => {
  const item = await Item.destroy({ truncate: true });
  return item;
};

const syncItem = async (id) => {
  const data = await axios({
    method: 'get',
    url: `https://hacker-news.firebaseio.com/v0/item/${id}.json`,
    responseType: 'json',
  });

  return data.data;
};

const getItem = async (id) => {
  let item = await Item.findOne({
    where: { id },
  });
  if (!item) {
    item = await syncItem(id);
    await createItem(itemProperties.create(item));
  }

  return itemProperties.get(item);
};

module.exports = {
  getAll,
  getItem,
  createItem,
  truncate,
  syncItem,
  getFavorite,
  putFavorite,
};
