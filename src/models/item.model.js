const { Model, DataTypes } = require('sequelize');
const sequelize = require('../config/database');

class Item extends Model {}

Item.init(
  {
    id: {
      type: DataTypes.INTEGER,
      unique: 'compositeIndex',
      primaryKey: true,
    },
    deleted: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
    type: {
      type: DataTypes.STRING,
    },
    by: {
      type: DataTypes.STRING,
    },
    time: {
      type: DataTypes.INTEGER,
    },
    text: {
      type: DataTypes.TEXT,
    },
    dead: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
    parent: {
      type: DataTypes.INTEGER,
    },
    poll: {
      type: DataTypes.INTEGER,
    },
    kids: {
      type: DataTypes.STRING,
    },
    url: {
      type: DataTypes.STRING,
    },
    score: {
      type: DataTypes.INTEGER,
    },
    title: {
      type: DataTypes.STRING,
    },
    parts: {
      type: DataTypes.STRING,
    },
    descendants: {
      type: DataTypes.INTEGER,
    },
    favorite: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
  },
  {
    sequelize,
    modelName: 'Item',
  }
);

module.exports = Item;
