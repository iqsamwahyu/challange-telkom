const { Model, DataTypes } = require('sequelize');
const sequelize = require('../config/database');

class TopStory extends Model {}

TopStory.init(
  {
    id: {
      type: DataTypes.INTEGER,
      unique: 'compositeIndex',
      primaryKey: true,
    },
  },
  {
    sequelize,
    modelName: 'TopStory',
  }
);

module.exports = TopStory;
