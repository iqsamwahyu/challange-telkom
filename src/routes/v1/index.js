const express = require('express');
const itemRoute = require('./item.route');
const topStoryRoute = require('./topstory.route');

const router = express.Router();

const defaultRoutes = [
  {
    path: '/item',
    route: itemRoute,
  },
  {
    path: '/topstories',
    route: topStoryRoute,
  },
];

defaultRoutes.forEach((route) => {
  router.use(route.path, route.route);
});

module.exports = router;
