const express = require('express');

const topStoryController = require('../../controllers/topstory.controller');

const router = express.Router();

router.get('/', topStoryController.getAll);
router.get('/sync', topStoryController.syncTopStory);

module.exports = router;
