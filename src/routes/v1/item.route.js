const express = require('express');

const { itemController } = require('../../controllers');

const router = express.Router();

router.get('/', itemController.getAll);
router.get('/clear', itemController.clearItems);
router.get('/favorite', itemController.getFavorite);
router.get('/:itemId', itemController.getItem);
router.put('/:itemId', itemController.putFavorite);

module.exports = router;
