const formatItem = require('../config/formatItem');

const create = (item) => {
  if (!item) {
    return;
  }
  if (item.hasOwnProperty('kids')) {
    item.kids = item.kids.toString();
  }
  if (item.hasOwnProperty('parts')) {
    item.parts = item.parts.toString();
  }
  if (!item.hasOwnProperty('favorite')) {
    item.favorite = false;
  }
  return item;
};

const get = (item) => {
  if (!item) {
    return;
  }
  if (item.kids) {
    item.kids = item.kids.split(',').map(Number);
  }
  if (item.parts) {
    item.parts = item.parts.split(',').map(Number);
  }

  // ask
  if (item.type === 'story' && item.hasOwnProperty('text')) {
    return formatItem.ASK(item);
  }
  // story
  if (item.type === 'story') {
    return formatItem.STORY(item);
  }
  // comment
  if (item.type === 'comment') {
    return formatItem.COMMENT(item);
  }
  // job
  if (item.type === 'job') {
    return formatItem.JOB(item);
  }
  // poll
  if (item.type === 'poll') {
    return formatItem.POLL(item);
  }
  // part
  if (item.type === 'pollopt') {
    return formatItem.PART(item);
  }

  return 'Something Went Wrong';
};

module.exports = {
  create,
  get,
};
