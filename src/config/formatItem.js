const ASK = (item) => {
  return {
    by: item.by,
    descendants: item.descendants,
    id: item.id,
    kids: item.kids,
    score: item.score,
    text: item.text,
    time: item.time,
    title: item.title,
    type: item.type,
    favorite: item.favorite,
  };
};
const STORY = (item) => {
  return {
    by: item.by,
    descendants: item.descendants,
    id: item.id,
    kids: item.kids,
    score: item.score,
    time: item.time,
    title: item.title,
    type: item.type,
    url: item.url,
    favorite: item.favorite,
  };
};
const COMMENT = (item) => {
  return {
    by: item.by,
    id: item.id,
    kids: item.kids,
    parent: item.parent,
    text: item.text,
    time: item.time,
    type: item.type,
    favorite: item.favorite,
  };
};
const JOB = (item) => {
  return {
    by: item.by,
    id: item.id,
    score: item.score,
    text: item.text,
    time: item.time,
    title: item.title,
    type: item.type,
    url: item.url,
    favorite: item.favorite,
  };
};
const POLL = (item) => {
  return {
    by: item.by,
    descendants: item.descendants,
    id: item.id,
    kids: item.kids,
    parts: item.parts,
    score: item.score,
    text: item.text,
    time: item.time,
    title: item.title,
    type: item.type,
    favorite: item.favorite,
  };
};
const PART = (item) => {
  return {
    by: item.by,
    id: item.id,
    poll: item.poll,
    score: item.score,
    text: item.text,
    time: item.time,
    type: item.type,
    favorite: item.favorite,
  };
};

module.exports = {
  ASK,
  STORY,
  COMMENT,
  JOB,
  POLL,
  PART,
};
