const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const { itemService } = require('../services');

const getAll = catchAsync(async (req, res) => {
  const items = await itemService.getAll();
  res.status(httpStatus.OK).send(items);
});

const getFavorite = catchAsync(async (req, res) => {
  const items = await itemService.getFavorite();
  res.status(httpStatus.OK).send(items);
});

const getItem = catchAsync(async (req, res) => {
  const item = await itemService.getItem(req.params.itemId);
  res.status(httpStatus.OK).send(item);
});

const putFavorite = catchAsync(async (req, res) => {
  const item = await itemService.putFavorite(req.params.itemId, req.body);
  res.status(httpStatus.OK).send(item);
});

const clearItems = catchAsync(async (req, res) => {
  await itemService.truncate();
  res.status(httpStatus.OK).send('Items cleared');
});

module.exports = {
  getAll,
  getItem,
  clearItems,
  getFavorite,
  putFavorite,
};
