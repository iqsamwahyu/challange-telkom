const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const { topStoryService } = require('../services');

const getAll = catchAsync(async (req, res) => {
  const item = await topStoryService.getAll();
  res.status(httpStatus.OK).send(item);
});

const syncTopStory = catchAsync(async (req, res) => {
  const item = await topStoryService.syncTopStory();
  res.status(httpStatus.OK).send(item);
});

module.exports = {
  getAll,
  syncTopStory,
};
