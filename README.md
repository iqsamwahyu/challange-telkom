## Manual Installation

1. Clone this repo

2. Install the dependencies:

```bash
yarn install
```
3. run
```
#DEV
yarn dev

#PROD
yarn start
```
## Commands

Running locally:

```bash
yarn dev
```

Running in production:

```bash
yarn start
```

Linting:

```bash
yarn lint
```

### API Endpoints

List of available routes:

**Item routes**:\
`GET /v1/item` - get all item\
`GET /v1/item/clear` - clear entire data item on database\
`GET /v1/item/favorite` - get all favorited item \
`GET /v1/item/:itemId` - get detail item\
`PUT /v1/item/:itemId`- {param: [favorite: true or false]} - update favorite on item\

**Top Story routes**:\
`GET /v1/topstories` - get all top stories\
`GET /v1/topstories/sync` - sync top stories from hacker news 

### TOOLS
* Express js
* Sequelize
* Axios
* pm2

### RESPONSE ENDPOINT
* Sync Top Stories from Hacker news\
![Sync-Top-Stories-from-Hacker-news](topstories-sync.jpg)

* Show All Top Stories\
![text](topstories.jpg)

* Show All item story in Database\
![Show-All-item-story-in-Database](all-item.jpg)

* Show item story detail by ID, sync from hacker news if not exist\
![Show-item-story-detail-by-ID](item-detail-1.jpg)
![Show-item-story-detail-by-ID](item-detail-2.jpg)
![Show-item-story-detail-by-ID](item-detail-3.jpg)

* Set Favorite in specific item story\
![Set-Favorite](put-favorite.jpg)

* Show all favorited item\
![Show-all-favorited-item](show-favorite.jpg)
